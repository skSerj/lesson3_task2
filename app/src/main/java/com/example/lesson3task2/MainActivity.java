package com.example.lesson3task2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText inputNumInBin;
    TextView numInBinary;
    EditText inputNumInOct;
    TextView numInOct;
    EditText inputNumInHex;
    TextView numInHex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputNumInBin = findViewById(R.id.input_text_bin);
        numInBinary = findViewById(R.id.num_in_binary);
        inputNumInOct = findViewById(R.id.input_text_oct);
        numInOct = findViewById(R.id.num_in_oct);
        inputNumInHex = findViewById(R.id.input_text_hex);
        numInHex = findViewById(R.id.num_in_hex);

        inputNumInBin.addTextChangedListener(binWatcher);
        inputNumInOct.addTextChangedListener(octWatcher);
        inputNumInHex.addTextChangedListener(hexWatcher);
    }

    private final TextWatcher binWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (inputNumInBin.getText().toString().length() == 0) {
                numInBinary.setText("entry number please");
            } else {
                int numberInBin = Integer.parseInt(inputNumInBin.getText().toString());
                String numbBin = Integer.toBinaryString(numberInBin);
                numInBinary.setText(numbBin);
            }
        }
    };

    private final TextWatcher octWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (inputNumInOct.getText().toString().length() == 0) {
                numInOct.setText("entry number please");
            } else {
                int numberInOct = Integer.parseInt(inputNumInOct.getText().toString());
                String numbOct = Integer.toOctalString(numberInOct);
                numInOct.setText(numbOct);
            }
        }
    };

    private final TextWatcher hexWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (inputNumInHex.getText().toString().length() == 0) {
                numInHex.setText("entry number please");
            } else {
                int numberInHex = Integer.parseInt(inputNumInHex.getText().toString());
                String numbHex = Integer.toHexString(numberInHex);
                numInHex.setText(numbHex);
            }
        }
    };
}

